



<img src="https://cool-show.oss-cn-shanghai.aliyuncs.com/admin/home-mini.png" alt="Admin Home" ></a>




## 安装项目依赖

推荐使用 `pnpm`：

```shell
pnpm i
```

## 运行应用程序

安装过程完成后，运行以下命令启动服务。您可以在浏览器中预览网站 [http://localhost:9000](http://localhost:9000)

```shell
pnpm dev
```

